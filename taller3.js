//Proceso de transformacion de hora actual a segundos:
function TransformarHora(){
    var algoS=document.getElementById("resultado");
    var Bminutos=60; var Bhoras=3600;

    var horaActual = new Date();
    var hora = horaActual.getHours();
    var minuto = horaActual.getMinutes();
    var segundo = horaActual.getSeconds();
    var SegunHora = hora * Bhoras;
    var SegunMinutos = minuto * Bminutos;
    var converR=parseInt(SegunMinutos) + parseInt(SegunHora) + parseInt(segundo);
    algoS.innerHTML = "Hora Actual:  " + hora + "-" + minuto + "-    " + segundo + "  Transformado:  " + converR;
}

//Calcular el area de un triangulo:
function obtenerArea(){
    var areaResultado = document.getElementById("resultado1");
    var b = parseInt(document.getElementById("base").value);
    var h = parseInt(document.getElementById("altura").value);

    var area = (b * h) / 2;
    areaResultado.innerHTML = "El area del triangulo es: " + area;
}

//Proceso para calcular la raiz cuadrada:
function RaizCuadrada(){
    var algo = document.getElementById("resultado2");
    var mensage = "Tiene que ser un numero impar ";
    var numero = document.getElementById("raiz1").value;
        if(numero %2){
            var resultado = Math.sqrt(numero);
            algo.innerHTML = "" + resultado.toFixed(2);
        }
        else{
            algo.innerHTML = "" + mensage;
        }
}

//Proceso para cualcular una cadena de texto:
function CadenaTexto(){
    var longitud = document.getElementById("resultado3");
    var entrada = document.getElementById("cadenaTexto").value;
    const newLocal = new String(entrada);
    var cadena = newLocal;
    longitud.innerHTML = "La longitud de la cadena de texto es:    " + cadena.length;
}

//Concatenar los arrays:  array1(Lunes, Martes, Miércoles, Jueves, Viernes) y array 2 (Sábado, Domingo):
function ConcatenarArray(){
    var resultadoSemana = document.getElementById("resultado4");
    var DiasLaborables = new Array(" Lunes", " Martes", " Miercoles", " Jueves", " Viernes");
    var FinSemana = new Array(" Sabado", " Domingo");
    var Semana = DiasLaborables.concat(FinSemana);
    resultadoSemana.innerHTML = `Resultado:   ${Semana}`;
}

//Mostrar la versión del navegador:
function VersionNav(){
    var version = document.getElementById("resultado5");
    version.innerHTML = "La version del Navegador es:   " + navigator.appVersion;
}

//Mostrar el ancho y la altura de la pantalla.:
function AnchoAltura(){
    var mostar = document.getElementById("resultado6");
    mostar.innerHTML = "Esta es la anchura de su pantalla:    " + screen.width + "   Esta es su altura:   " + screen.height;
}

//Imprimir la página.:
function ImprimirPagina(){
    if(window.print){
        window.print();
    }
}
